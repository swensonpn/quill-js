var path = require('path');

const config = {
	entry: './app/index.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname,'./dist')
	},
	resolve:{
		alias: {			
			'quill': path.resolve(__dirname,'node_modules/quill/dist/')
		}
	},
	module: {
		loaders: [
			{test: /\.css$/, loader: 'style-loader!css-loader'}
			/*{
				test: /\.css$/,
				loader: 'style!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
			}*/
		]
	}
	
/*	
	resolve: {
		alias: {
			'parchment': path.resolve(__dirname, 'node_modules/parchment/src/parchment.ts'),
			'quill$': path.resolve(__dirname, 'node_modules/quill/quill.js')
		},
		extensions: ['.js', '.ts', '.svg']
	},
	module:{
		loaders: [{
			test: /\.js$/,
			loader: 'babel-loader',
			query: {
				presets: ['es2015']
			}
		}, 
		{
			test: /\.ts$/,
			loader: 'ts'
		}, 
		{
			test: /\.svg$/, loader: 'html?minimize=true'
		}]
	}
	*/
}

module.exports = config;