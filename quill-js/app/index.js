/**
 * Module Configuration
 */
const config = {
	theme: 'bubble',
	selector: '#editor',
	saveInterval:5000
}

/**
 * Imports/Loaders
 */
var Theme = require("quill/quill." + config.theme + ".css");
var Quill = require("quill/quill.js");
var Delta = Quill.import('delta');

/**
 * Create Quill Instance
 */
var quill = new Quill(config.selector, {
    theme: config.theme,
    modules:{
    	toolbar:[
    		['bold','italic','underline','strike'],
    		[
    			{'size':['small',false,'large','huge']}
    		],
    		[
    			{'list':'ordered'},
    			{'list':'bullet'}
    		],
    		[
    			['link']
    		],
    		[
    			{'indent': '+1'},
    			{'indent': '-1'}
    			
    		],
    		['clean']
    	]
    }
 });

/**
 * Encode HTML Entities - do I need this?
 */
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/** 
 * Capture accumulated changes
 */
var change = new Delta();
quill.on('text-change', function(delta) {
  change = change.compose(delta);
  console.log(JSON.stringify(quill.getContents()))
});

/**
 * Periodic Saving
 */
setInterval(function(){
	if (change.length() > 0) {
	    console.log('Saving changes', change);
	    /* 
	    Send partial changes
	    $.post('/your-endpoint', { 
	      partial: JSON.stringify(change) 
	    });
	    
	    Send entire document
	    $.post('/your-endpoint', { 
	      doc: JSON.stringify(quill.getContents())
	    });
	    */
	    change = new Delta();
	  }
},config.saveInterval);

/**
 * Notify if page change without saving
 */
window.onbeforeunload = function() {
  if (change.length() > 0) {
    return 'There are unsaved changes. Are you sure you want to leave?';
  }
}
